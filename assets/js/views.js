class Views{
    constructor(){
        this.yellowPos = [0, 0]
        this.bluePos = [0, 0]
        this.redPos = [0, 0]
    }

    getTd(x ,y){    // Retourne la cases correspondante aux coordonnées x, y
        // En une ligne : return document.getElementById("plateau").childNodes[1].childNodes[2 * pos[1]].childNodes[1 + 2 * pos[0]]
        if(x >= 0 && x < 8 && y >= 0 && y < 8){
            let plateau = document.getElementById("plateau")
            let trArray = plateau.childNodes[1].childNodes
            let tr = trArray[2 * y]
            let tdArray = tr.childNodes
            let td = tdArray[1 + 2 * x]
            return td
        }
    }

    getDamierColor(x, y){
        let color
        let position = 8 * y + x - (y % 2)
        position % 2 == 1 ? color = "sienna" : color = "moccasin"
        return color
    }

    updateAnnonce(colorId){      // Mettre à jour l'annonce du tour du joueur
        let joueur
        colorId == 0 ? joueur = "noir" : joueur = "blanc"
        document.getElementById("Annonce").innerText = "C'est au joueur " + joueur + " de jouer"
    }

    updatePlateau(pawnTab){      // Mettre les images des pions dans les cases correspondantes
        let plateau = document.getElementById("plateau")
        let trArray = plateau.childNodes[1].childNodes
        for (let tr of trArray){
            if(tr.nodeType != 3){
                let tdArray = tr.childNodes
                for (let td of tdArray) {
                    if(td.nodeType != 3){
                        let pawn = pawnTab[tr.id - 1][td.id - 1]
                        if(pawn) td.childNodes[0].src = "../TexturesPions/" + pawn.role + pawn.color + ".png"
                    }
                }
            }
        }
    }

    updateSelected(pos){
        if(pos && pos[0] >= 0 && pos[1] >= 0){
            // Reset blue
            let td
            if(this.bluePos){
                td = this.getTd(this.bluePos[0] ,this.bluePos[1])
                td.style.background = this.getDamierColor(this.bluePos[0], this.bluePos[1]) 
            }
            // Set yellow
            td = this.getTd(pos[0], pos[1])
            td.style.background = "yellow"
            this.yellowPos = pos
        }
    }

    updatePlayed(pos){
        if(pos && pos[0] >= 0 && pos[1] >= 0){
            // Reset yellow
            let td = this.getTd(this.yellowPos[0] ,this.yellowPos[1])
            td.style.background = this.getDamierColor(this.yellowPos[0], this.yellowPos[1])
        
            // Set blue
            td = this.getTd(pos[0], pos[1])
            if(td) td.style.background = "blue"
            this.bluePos = pos
        }
    }

    updateRed(pos){
        if(pos && pos[0] >= 0 && pos[1] >= 0){
            // Remove old 
            let td = this.getTd(this.redPos[0], this.redPos[1])
            td.style.background = this.getDamierColor(this.redPos[0], this.redPos[1])
            this.redPos = pos

            // Set new
            td = this.getTd(pos[0], pos[1])
            if(td) td.style.background = "red"
            this.redPos = pos
        }
    }

    deselect(){
        let td = this.getTd(this.yellowPos[0], this.yellowPos[1])
        td.style.background = this.getDamierColor(this.yellowPos[0], this.yellowPos[1])
    }

    checkmate(){
        document.getElementById("Annonce").innerHTML = "Échec et mat !!!"
        document.getElementById("Annonce").style.fontSize = 150
        document.getElementById("Annonce").style.textAlign = "center"
    }

    updateViews(gameInfo) {
        this.updateAnnonce(gameInfo[1])
        this.updatePlateau(gameInfo[0])
        this.updatePlayed(gameInfo[3])
    }

    updateWin(color) {
        let joueur
        color == 1 ? joueur = "noir" : joueur = "blanc"
        let text = "Le joueur " + joueur + " à gagné !"
        document.getElementById("Annonce").innerText = text
        alert(text)
        console.log("== Win !! ==")
    }
}