(function () {
    let url = window.location.href; 
    let playerID = url[url.length - 1]

    const socket = io.connect('http://127.0.0.1:8080')
    let views = new Views()
    socket.on('updateViews', views.updateViews.bind(views))
    socket.on('selected', views.updateSelected.bind(views))
    socket.on('setRed', views.updateRed.bind(views))
    socket.on('deselect', views.deselect.bind(views))
    socket.on('checkMate', views.checkmate.bind(views))

    function tdClicked(x, y, playerID) {
        console.log('played at x : ' + x + ' y : ' + y + ' id : ' + playerID)
        socket.emit('played', [x - 1, y - 1, playerID])
    }

    function initialiseDamier() {
        let trArray = document.getElementsByTagName("tr")
        for (let tr of trArray) {
            let tdArray = tr.childNodes
            for (let td of tdArray) {
                if (td.nodeType != 3) {     // Certain index sont des #text, on les esquive
                    td.addEventListener('click', tdClicked.bind(td, td.id, tr.id, playerID - 1));
                    let position = 8 * tr.id + td.id - (tr.id % 2)
                    position % 2 == 0 ? td.style.background = "moccasin" : td.style.background = "sienna"
                    let img = document.createElement("img")
                    img.src = "../TexturesPions/0-1.png"
                    img.height = 60
                    img.width = 60
                    td.appendChild(img)
                }
            }
        }
        console.log("Initialized Damier")
    }

    initialiseDamier()
})()
