const Pion = require('./Pion')

class Game{
    constructor(socket) {
        this.colorTurn = 1
        this.pawnTab = new Array(8)
        this.pawnSelected = false
        this.lastClick = [-1, -1]
        this.socket = socket
        this.resetTab()
    }

    getInfo(){
        return [this.pawnTab, this.colorTurn, this.pawnSelected, this.lastClick]
    }

    handleClick(x ,y, id) {
        if(this.pawnSelected) {        // Si un pion est deja séléctionné
            if(this.move(this.lastClick[0], this.lastClick[1], x , y)) {    // Si le pion a bien été déplacé, on le lache
                this.pawnSelected = false
            }
            else if(this.lastClick[0] == x && this.lastClick[1] == y) {      // Reclique sur la même case, on lache
                this.socket.emit('deselect')
                this.pawnSelected = false
            }
        }
        else if(this.choosePawn(x, y).color == id){                           // Si l'utilisateur à cliqué sur un de ses pions
            this.pawnSelected = (this.choosePawn(x, y).color == this.colorTurn)
            if(this.pawnSelected) {
                this.socket.emit('selected',[x, y])
                this.lastClick = [x, y]
            }
        }
    }

    resetTab() {
        for (let i = 0; i < 8; i++) {
            this.pawnTab[i] = new Array(8)
        }
        for (let j = 0; j < 8; j++){
            for (let i = 2; i < 6; i++){
                this.pawnTab[i][j] = new Pion(0,-1)
            }
        }
        for (let x = 0; x < 8; x++) {
            this.pawnTab[1][x] = new Pion(1, 0) // Crée ligne pion noir
            this.pawnTab[6][x] = new Pion(1, 1) // Crée ligne pion blanc
        }
        for (let i = 0; i < 3; i++) {
            this.pawnTab[0][i] = new Pion(i + 2, 0) // Crée ligne TCF noir
            this.pawnTab[7][i] = new Pion(i + 2, 1) // Crée ligne TCF blanc    
        }
        for (let i = 0; i < 3; i++) {
            this.pawnTab[0][7 - i] = new Pion(i + 2, 0) // Crée ligne CFT noir
            this.pawnTab[7][7 - i] = new Pion(i + 2, 1) // Crée ligne CFT blanc    
        }
        this.pawnTab[0][3] = new Pion(6, 0)
        this.pawnTab[0][4] = new Pion(5, 0) // Roi et dames
        this.pawnTab[7][3] = new Pion(6, 1)
        this.pawnTab[7][4] = new Pion(5, 1)

        this.socket.emit('updateViews', this.getInfo())
    }

    choosePawn(x ,y) { // Vérifier quelle image contient le tab sur lequel on clique et affecter une valeur à pion en conséquence. 
        return this.pawnTab[y][x]
    }

    testPion(x,y,role,color){
        if (x >= 0 && x <= 7 && y >= 0 && y <= 7) {
            if (this.choosePawn(x,y).role == role && this.choosePawn(x,y).color == color) {
                return true
            }
        }
        
    }

    //fonction qui valide les déplacement en fonction du role du pion
    roleMove(x, y, toX, toY) {
        let isMovable = false
        let pion = this.choosePawn(x,y)
        let pionTarget = this.choosePawn(toX, toY)

        // empeche de jouer sur son propre pion
        if (pionTarget.color == pion.color) {
            return false
        }
        switch (pion.role) {
            case 1: //Pion
                //les pions noirs descendent forcément
                if (pion.color == 0) {
                    //avancée de 1
                    if (y + 1 <= 7 && toY == y + 1) {
                        //tout droit
                        if (toX == x && pionTarget.role == 0) {
                            isMovable = true
                        }
                        //diagonale avant si présence de pion ennemi
                        if ((toX == x - 1 || toX == x + 1) && pionTarget.color == (pion.color+1) % 2) {
                            isMovable = true
                        }
                    }
                    //avancée de 2 cases depuis la position de démarrage
                    else if (toY == y + 2 && toX == x && y == 1 && pionTarget.role == 0 && this.choosePawn(toX, toY-1).role == 0) {
                        isMovable = true
                    }
                }
                
                //les pions blancs montent forcément
                else if (pion.color == 1) {
                    //avancée de 1 case
                    if (y - 1 >= 0 && toY == y - 1) {
                        //tout droit
                        if (toX == x && pionTarget.role == 0) {
                            isMovable = true
                        }
                        //en diagonale si présence de pion ennemi
                        if ((toX == x - 1 || toX == x + 1) && pionTarget.color == (pion.color+1) % 2) {
                            isMovable = true
                        }
                    }
                    //avancée de 2 cases depuis la position de démarrage
                    else if (toY == y - 2 && toX == x && y == 6  && pionTarget.role == 0 && this.choosePawn(toX, toY+1).role == 0) {
                        isMovable = true
                    }
                }
                break;
                
                case 2: //Tour
                // dans chaque cas on vérifie qu'il n'y a pas de pion sur le deplacement demandé

                //Teste l'allignement des deux cases (Vertical ou Horizontal)
                if (x== toX || y == toY) {  
                    let obstacle = false
                    //Mouvement Vertical
                    if(x == toX && y != toY ){
                        //Bas
                        if (toY > y) {                        
                            let j = y
                            do {
                                if (this.choosePawn(x,j).color != -1 && j != y) {
                                    obstacle = true
                                }
                                j++
                            } while (j != toY);
                        }
                        //Haut
                        else{  
                            let j = y
                            do {
                                if (this.choosePawn(x,j).color != -1 && j != y) {
                                    obstacle = true
                                }
                                j--
                            } while (j != toY);
                        }
                    }

                    //Mouvement Horizontal
                    if (y == toY && x != toX ){
                        // Droite
                        if (toX > x) {                        
                            let j = x
                            do {
                                if (this.choosePawn(j,y).color != -1 && j != x) {
                                    obstacle = true
                                }
                                j++
                            } while (j != toX);                   
                        }

                        //Gauche
                        else{
                            let j = x
                            do {
                                if (this.choosePawn(j,y).color != -1 && j != x) {
                                    obstacle = true
                                }
                                j--
                            } while (j != toX); 
                        }
                    }
                    if (obstacle == false) {
                        isMovable = true
                    }
                }
                
                break;

            case 3: //cavalier
                if(toY == y + 2 || toY == y - 2 ) {  //coup en bas ou en haut
                    if(toX == x + 1 || toX == x - 1 ) {
                        isMovable = true            
                    }
                }
                
                else if(toX == x + 2 || toX == x - 2 ) {  //coup à gauche ou à droite
                    if(toY == y + 1 || toY == y - 1 ) {
                        isMovable = true            
                    }
                }
            break;

            case 4: //fou
                if(x != toX && y != toY && Math.abs(toX - x) == Math.abs(toY - y)) {
                    let obstacle = false
                    //Bas
                    if (toY > y) {
                        //Droite
                        if (toX > x) {
                            let i = x
                            let j = y
                            do {
                                if (this.choosePawn(i,j).color != -1 && i != x && j != y) {
                                    obstacle = true
                                }
                                i++
                                j++
                            } while(i != toX && j != toY)           
                        }
                        //Gauche
                        else {
                            let i = x
                            let j = y
                            do {
                                if (this.choosePawn(i,j).color != -1 && i != x && j != y) {
                                    obstacle = true
                                }
                                i--
                                j++
                            } while(i != toX && j != toY)
                        }
                    }
                    //Haut
                    else{
                        // Droite
                        if (toX > x) {
                            let i = x
                            let j = y
                            do {
                                if(this.choosePawn(i,j).color != -1 && i != x && j != y){
                                    obstacle = true
                                }
                                i++
                                j--
                            } while(i != toX && j != toY)            
                        }
                        //Gauche
                        else {
                            let i = x
                            let j = y
                            do {
                                if(this.choosePawn(i,j).color != -1 && i != x && j != y){
                                    obstacle = true
                                }
                                i--
                                j--
                            } while(i != toX && j != toY)   
                        }
                    }
                    if (obstacle == false) {
                        isMovable = true
                    }
                }
                break;

            case 5: //roi
                if (!this.isCheck(toX, toY, this.colorTurn)) {
                    if(Math.abs(toX - x) <= 1 && Math.abs(toY - y) <= 1) {
                        if(toY == y+1 && toX == x + 1) {  //Diago bas droite
                            isMovable = true
                        }
                        else if(toY == y+1 && toX == x) {   //Verticale bas
                            isMovable = true
                        }
                        else if(toY == y+1 && toX == x - 1) {    //Diago bas gauche
                            isMovable = true
                        }
                        else if(toY == y && toX == x - 1) {   //Horizontale gauche
                            isMovable = true
                        }
                        else if(toY == y-1 && toX == x - 1) {     //Diago Haut Gauche
                            isMovable = true
                        }
                        else if(toY == y-1 && toX == x) {   //Verticale haut
                            isMovable = true
                        }
                        else if(toY == y+1 && toX == x + 1) {     //Diago Haut droite
                            isMovable = true
                        }
                        else if(toY == y && toX == x + 1) {   //Horizontale droite  
                            isMovable = true
                        }
                    } 
                }
                else{
                    isMovable = false
                }
                break;

            case 6: //dame
                // La dame a le même comportement que la tour et le fou
                //Teste l'allignement des deux cases (Vertical ou Horizontal)
                if (x== toX || y == toY) {   
                    let obstacle = false
                    //Mouvement Vertical
                    if(x == toX && y != toY ){
                        //Bas
                        if (toY > y) {                        
                            let j = y
                            do {
                                if (this.choosePawn(x,j).color != -1 && j != y) {
                                    obstacle = true
                                }
                                j++
                            } while (j != toY);
                        }
                        //Haut
                        else{
                            
                            let j = y
                            do {
                                if (this.choosePawn(x,j).color != -1 && j != y) {
                                    obstacle = true
                                }
                                j--
                            } while (j != toY);
                        }
                    }

                    //Mouvement Horizontal
                    if (y == toY && x != toX ){
                        // Droite
                        if (toX > x) {                        
                            let j = x
                            do {
                                if (this.choosePawn(j,y).color != -1 && j != x) {
                                    obstacle = true
                                }
                                j++
                            } while (j != toX);                   
                        }

                        //Gauche
                        else{
                            let j = x
                            do {
                                if (this.choosePawn(j,y).color != -1 && j != x) {
                                    obstacle = true
                                }
                                j--
                            } while (j != toX); 
                        }
                    }

                    if (obstacle == false) {
                        isMovable = true
                    }
                }

                if(x != toX && y != toY && Math.abs(toX - x) == Math.abs(toY - y)) {
                    let obstacles = false
                    //Bas
                    if (toY > y) {
                        //Droite
                        if (toX > x) {
                            let i = x
                            let j = y
                            do {
                                if (this.choosePawn(i,j).color != -1 && i != x && j != y) {
                                    obstacles = true
                                }
                                i++
                                j++
                            } while(i != toX && j != toY)           
                        }
                        //Gauche
                        else {
                            let i = x
                            let j = y
                            do {
                                if (this.choosePawn(i,j).color != -1 && i != x && j != y) {
                                    obstacles = true
                                }
                                i--
                                j++
                            } while(i != toX && j != toY)
                        }
                    }

                    //Haut
                    else{
                        // Droite
                        if (toX > x) {
                            let i = x
                            let j = y
                            do {
                                if(this.choosePawn(i,j).color != -1 && i != x && j != y){
                                    obstacles = true
                                }
                                i++
                                j--
                            } while(i != toX && j != toY)            
                        }
                        //Gauche
                        else {
                            let i = x
                            let j = y
                            do {
                                if(this.choosePawn(i,j).color != -1 && i != x && j != y){
                                    obstacles = true
                                }
                                i--
                                j--
                            } while(i != toX && j != toY)   
                        }
                    }
                    if (obstacles == false) {
                        isMovable = true
                    }
                }
                break;

            default:
                break;
        }
        return isMovable;
    }

    // on regarde si le roi en face a été mangé
    // return false lorsque le roi est encore présent
    hasWin(color) {
        for (let y = 0; y < 8; y++) {
            for (let x = 0; x < 8; x++) {
                if(this.testPion(x, y, 5, (color + 1) % 2)) {
                    return false
                }
            }
        }
        return true
    }

    //colorTurn
    isCheck(x, y, color) {
        // Regarde le long du board si le roi est en danger
        let check = false
        let yK;
        let xK;

        // Verticale bas
        yK = y
        while ((this.choosePawn(x, yK).role == 0 || this.testPion(x, yK, 5, this.colorTurn)) && yK < 7) {
            yK++
            if ((this.testPion(x, yK, 2, (this.colorTurn + 1) % 2)|| this.testPion(x, yK, 6, (this.colorTurn + 1) % 2)) && yK != y) {
                check = true
            }
        }

        // Verticale haute
        yK = y
        while ((this.choosePawn(x, yK).role == 0 || this.testPion(x, yK, 5, this.colorTurn)) && yK > 0) {
            yK--
            if ((this.testPion(x, yK, 2, (this.colorTurn + 1) % 2)|| this.testPion(x, yK, 6, (this.colorTurn + 1) % 2)) && yK != y) {
                check = true
            }
        }

        // Horizontale droite
        xK = x
        while ((this.choosePawn(xK, y).role == 0 || this.testPion(xK, y, 5, this.colorTurn)) && xK < 7) {
            xK++
            if ((this.testPion(xK, y, 2, (this.colorTurn + 1) % 2)|| this.testPion(xK, y, 6, (this.colorTurn + 1) % 2)) && xK != x) {
                check = true
            }
        }

        // Horizontale gauche 
        xK = x
        while ((this.choosePawn(xK, y).role == 0 || this.testPion(xK, y, 5, this.colorTurn)) && xK > 0) {
            xK--
            if ((this.testPion(xK, y, 2, (this.colorTurn + 1) % 2)|| this.testPion(xK, y, 6, (this.colorTurn + 1) % 2)) && xK != x) {
                check = true
            }
            
        }

        // Diagonale haut gauche 
        xK = x
        yK = y
        while ((this.choosePawn(xK, yK).role == 0 || this.testPion(xK, yK, 5, this.colorTurn)) && xK > 0 && yK > 0) {
            xK--
            yK--
            if ((this.testPion(xK, yK, 4, (this.colorTurn + 1) % 2)|| this.testPion(xK, yK, 6, (this.colorTurn + 1) % 2)) && xK != x && yK != y) {
                check = true
            } 
        }

        // Diagonale haut droite 
        xK = x
        yK = y
        while ((this.choosePawn(xK, yK).role == 0 || this.testPion(xK, yK, 5, this.colorTurn)) && xK < 7 && yK > 0) {
            xK++
            yK--
            if ((this.testPion(xK, yK, 4, (this.colorTurn + 1) % 2)|| this.testPion(xK, yK, 6, (this.colorTurn + 1) % 2)) && xK != x && yK != y) {
                check = true
            } 
        }

        // Diagonale bas gauche 
        xK = x
        yK = y
        while ((this.choosePawn(xK, yK).role == 0 || this.testPion(xK, yK, 5, this.colorTurn)) && xK > 0 && yK < 7) {
            xK--
            yK++
            if ((this.testPion(xK, yK, 4, (this.colorTurn + 1) % 2)|| this.testPion(xK, yK, 6, (this.colorTurn + 1) % 2)) && xK != x && yK != y) {
                check = true
            } 
        }

        // Diagonale bas droite 
        xK = x
        yK = y
        while ((this.choosePawn(xK, yK).role == 0 || this.testPion(xK, yK, 5, this.colorTurn)) && xK < 7 && yK < 7) {
            xK++
            yK++
            if ((this.testPion(xK, yK, 4, (this.colorTurn + 1) % 2)|| this.testPion(xK, yK, 6, (this.colorTurn + 1) % 2)) && xK != x && yK != y) {
                check = true
            } 
        }

        // Cavalier 
        if (this.testPion(x-1,y-2,3,(this.colorTurn+1)%2) || this.testPion(x+1,y-2,3,(this.colorTurn+1)%2) || this.testPion(x-2,y-1,3,(this.colorTurn+1)%2) || this.testPion(x+2,y-1,3,(this.colorTurn+1)%2)|| this.testPion(x-2,y+1,3,(this.colorTurn+1)%2) || this.testPion(x+2,y+1,3,(this.colorTurn+1)%2) || this.testPion(x-1,y+2,3,(this.colorTurn+1)%2) || this.testPion(x+1,y+2,3,(this.colorTurn+1)%2)) {
            check = true
        }

        // Pion
        if(color == 0 || color == 1) {
            if(this.testPion(x - 1 ,y + 1, 1, (color + 1) % 2) || this.testPion(x + 1, y + 1, 1, (color + 1) % 2)) {
                check = true
            }
        }

        // Roi ennemi (au cas ou)
        if(this.testPion(x-1,y-1,5,(this.colorTurn+1)%2) || this.testPion(x,y-1,5,(this.colorTurn+1)%2) || this.testPion(x+1,y-1,5,(this.colorTurn+1)%2)|| this.testPion(x-1,y,5,(this.colorTurn+1)%2)|| this.testPion(x+1,y,5,(this.colorTurn+1)%2)|| this.testPion(x-1,y+1,5,(this.colorTurn+1)%2)|| this.testPion(x,y+1,5,(this.colorTurn+1)%2)|| this.testPion(x+1,y+1,5,(this.colorTurn+1)%2))  {
            check = true
        }
        return check
    }
    
    findKing(color){  // Trouver le roi de la couleur spécifiée 
        for (let y = 0; y < 8; y++) {
            for (let x = 0; x < 8; x++) {
                if(this.testPion(x, y, 5, color)){
                    return [x, y]
                }
            }
        }
    }

    onePossibleMove(){  // Est ce que le joueur à un déplacement possible
        let canMove = false;
        let kingPos;
        for (let y = 0; y < 8; y++) {
            for (let x = 0; x < 8; x++) {
                for (let toY = 0; toY < 8; toY++) {
                    for (let toX = 0; toX < 8; toX++) {
                        if(this.choosePawn(x, y).color == this.colorTurn && this.roleMove(x, y, toX, toY)){
                            // Sauvegarder l'emplacement ou va aller le pion
                            let oldPawn = new Pion(this.pawnTab[toY][toX].role , this.pawnTab[toY][toX].color)
                            // Déplacer le pion
                            this.pawnTab[toY][toX].color = this.pawnTab[y][x].color
                            this.pawnTab[toY][toX].role = this.pawnTab[y][x].role
                            this.pawnTab[y][x].role = 0
                            this.pawnTab[y][x].color = -1
                            // Reprendre la position du roi
                            kingPos = this.findKing(this.colorTurn)

                            if(!this.isCheck(kingPos[0], kingPos[1])) {  // S'il y a toujours échec
                                canMove = true
                            } 
                            // Annuler le dernier coup
                            this.pawnTab[y][x].color = this.pawnTab[toY][toX].color
                            this.pawnTab[y][x].role = this.pawnTab[toY][toX].role
                            this.pawnTab[toY][toX].color = oldPawn.color
                            this.pawnTab[toY][toX].role = oldPawn.role
                        }
                    }
                }
            }
        }
        return canMove
    }

    move(x, y, toX, toY) {
        let canMove = this.roleMove(x, y, toX, toY)
        let kingPos = this.findKing(this.colorTurn)

        if(!this.onePossibleMove()){
            this.socket.emit('checkMate')
        }
        else{
            if(canMove && this.isCheck(kingPos[0], kingPos[1])){ // Si le mouvement est possible et qu'il y a échec
                // Sauvegarder l'emplacement ou va aller le pion
                let oldPawn = new Pion(this.pawnTab[toY][toX].role , this.pawnTab[toY][toX].color)
                // Déplacer le pion
                this.pawnTab[toY][toX].color = this.pawnTab[y][x].color
                this.pawnTab[toY][toX].role = this.pawnTab[y][x].role
                this.pawnTab[y][x].role = 0
                this.pawnTab[y][x].color = -1
                // Reprendre la position du roi
                kingPos = this.findKing(this.colorTurn)

                if(this.isCheck(kingPos[0], kingPos[1])){ // S'il y a toujours échec
                    // Annuler le dernier coup
                    this.pawnTab[y][x].color = this.pawnTab[toY][toX].color
                    this.pawnTab[y][x].role = this.pawnTab[toY][toX].role
                    this.pawnTab[toY][toX].color = oldPawn.color
                    this.pawnTab[toY][toX].role = oldPawn.role
                    // Le coup est interdit
                    return false
                }
                else{ // Sinon le coup est autorisé
                    this.colorTurn += 1
                    this.colorTurn = this.colorTurn % 2
                    this.socket.emit('updateViews', this.getInfo())
                    this.socket.emit('setRed',[toX, toY])
                    return canMove
                }
            }
            else if (canMove) { // Peut aller ici et il n'y a pas échec
                this.pawnTab[toY][toX].color = this.pawnTab[y][x].color
                this.pawnTab[toY][toX].role = this.pawnTab[y][x].role
                this.pawnTab[y][x].role = 0
                this.pawnTab[y][x].color = -1
                this.colorTurn += 1
                this.colorTurn = this.colorTurn % 2
                this.socket.emit('updateViews', this.getInfo())
                this.socket.emit('setRed', [toX, toY])
            }
            else{
                console.log("Couldn't move")
            }
        }
        if(this.hasWin((this.colorTurn + 1) % 2)) this.socket.emit('win', this.colorTurn); this.socket.emit('reset')
        return canMove
    }
}

module.exports = Game