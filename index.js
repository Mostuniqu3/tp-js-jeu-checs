const port = 8080;

const express = require('express');
const app = express();                             // Routage
const server = require('http').createServer(app);  // Serveur
const io = require('socket.io')(server);           // SocketIO

const Game = require('./server_modules/game')

var game = new Game(io)

app.use(express.static(__dirname + "/assets/"));

app.get("/", (req , res, next) => {
    res.sendFile(__dirname + '/assets/views/prematch.html');     // Données envoyées du front
});

app.get("/player1", (req , res, next) => {
    res.sendFile(__dirname + '/assets/views/index.html');       // Données envoyées du front
});

app.get("/player2", (req , res, next) => {
    res.sendFile(__dirname + '/assets/views/index.html');       // Données envoyées du front
});

io.sockets.on('connection', (socket) => {                       // connection et disconnect existent deja

    socket.emit('updateViews', game.getInfo())

    socket.on('played', (pos) => {
        game.handleClick(pos[0], pos[1], pos[2])  // pos[2] est l'id du joueur
    });
});

server.listen(port);

console.log('Server instantiated');
